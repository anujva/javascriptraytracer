/*
 * Owner: Anuj Varma
 * 
 * This Class defines a ray
 * 
 */

function Ray(origin, direction){
	this.origin = origin;
	this.direction = direction;
}
	
Ray.prototype.rayPoints = function(t){
	return Vector3d.addScale(this.origin, this.direction, t);
}

