/*
 * Owner: Anuj Varma
 * 
 * This defines the number of Samples that will be used to calculate the value of the color for each pixel
 * 
 */

function Depth(numberOfSamples){
	this.numSamples = numberOfSamples;
	this.samplesRemaining = numberOfSamples;
	this.resultantColor = new Vector3d();
}

Depth.prototype.hasNext = function(){
	return (this.samplesRemaining>0);
}

Depth.prototype.colorResult = function(){
	return this.resultantColor;
}

Depth.prototype.getNext = function(){
	if(this.samplesRemaining>0){
		this.samplesRemaining -= 1;
		this.weight = 1/this.numSamples;
		return {
			x: 0,
			y: 0
		}
	}
}

Depth.prototype.reset = function(){
	this.samplesRemaining = this.numSamples;
	this.resultantColor = new Vector3d(0, 0, 0);
}

Depth.prototype.addSamples = function(col){
	this.resultantColor.addScale(col, this.weight);
}
