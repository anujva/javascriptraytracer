/*
 * Owner: Anuj Varma
 * 
 * This file defines the Plane object and will be used for the intersection and normal 
 * 
 */

function Plane(nor, dist, material){
	Objects.call(this, material);
	this.normal  = nor|| new Vector3d(0,1,0);
	this.distance = dist||0;
}

Plane.prototype = new Objects();

Plane.prototype.intersection = function(r){
	var a = r.direction.dotProduct(this.normal);
	if(Math.abs(a) <= 1e-2){
		return undefined;
	}else{
		var b = -(this.normal.dotProduct(r.origin) - this.distance);
		// console.log(this.normal);
		// console.log(r.origin);
		var t = b/a;
		// console.log("B value: "+b);
		// console.log("A value:" +a);
		// console.log("T values: "+t);
		if(t<0){
			//ray and plane intersection point lies behind the ray origin
			return undefined;
		}
		
		return {
			t: t,
			normal: this.normal
		}
	}
}
