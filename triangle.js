/*
 * Owner: Anuj Varma
 * 
 * This file defines the Triangle object and will be used for the intersection and normal 
 * 
 */

function Triangle(a, b, c, material){
	Objects.call(this, material);
	this.A = a||new Vector3d(1,0,0);
	this.B = b||new Vector3d(0,1,0);
	this.C = c||new Vector3d(0,0,1);
	this.normal  = this.getNormal();
	this.distance = this.getDist();
}

Triangle.prototype = new Objects();

Triangle.prototype.getNormal = function(){
	var CA = new Vector3d(this.C.x - this.A.x, this.C.y- this.A.y, this.C.z- this.A.z);
	var BA = new Vector3d(this.B.x - this.A.x, this.B.y - this.A.y, this.B.z - this.A.z);
	normal = CA.crossProduct(BA).normalize();
	return normal;
}

Triangle.prototype.getDist = function(){
	return this.normal.dotProduct(this.A);	
}

Triangle.prototype.intersection = function(r){
	//float t = - (dot(triNormal, rayOrigin) + D) / dot(triNormal, rayDirection);
	
	var a = r.direction.dotProduct(this.normal);
	
	if(Math.abs(a) <= 1e-2){
		return undefined;
	}else{
		var b = -(this.normal.dotProduct(r.origin) - this.distance);
		//console.log(r.origin);
		var t = b/a;
		
		if(t<=0){
			//ray and plane intersection point lies behind the ray origin
			return undefined;
		}
		
		if(t == Infinity){
			return undefined;
		}
		
		//now we can find the point of intersection of the plane
		//console.log(t);
		var pointOntriPlane = r.origin.addVec(r.direction.scale(t));
		//console.log(pointOntriPlane);
		var CA = new Vector3d(this.C.x - this.A.x, this.C.y- this.A.y, this.C.z- this.A.z);
		var BC = new Vector3d(this.B.x - this.C.x, this.B.y- this.C.y, this.B.z- this.C.z);
		var AB = new Vector3d(this.A.x - this.B.x, this.A.y- this.B.y, this.A.z- this.B.z);
		
		var PA = new Vector3d(pointOntriPlane.x - this.A.x, pointOntriPlane.y- this.A.y, pointOntriPlane.z- this.A.z);
		var PB = new Vector3d(pointOntriPlane.x - this.B.x, pointOntriPlane.y- this.B.y, pointOntriPlane.z- this.B.z);
		var PC = new Vector3d(pointOntriPlane.x - this.C.x, pointOntriPlane.y- this.C.y, pointOntriPlane.z- this.C.z);
		
		var test1 = this.normal.dotProduct(CA.crossProduct(PC));
		var test2 = this.normal.dotProduct(AB.crossProduct(PA));
		var test3 = this.normal.dotProduct(BC.crossProduct(PB));
		
		if(test1>0 && test2>0 && test3>0){
			return {
				t: t,
				normal: this.normal
			}
		} else{
			return undefined;
		}
		
	}
}
