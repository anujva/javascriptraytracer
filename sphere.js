/*
 * Owner: Anuj Varma
 * 
 * This file defines the Object Sphere, its intersection routine and the normal to its surface
 * 
 */

function Sphere(c, rad, material){
	Objects.call(this, material);
	this.center = c;
	this.radius = rad;
}

Sphere.prototype = new Objects();

Sphere.prototype.intersection = function(r){
	var d = Vector3d.subtract(r.origin, this.center);
	
	//calculate the discriminant
	var a = Vector3d.dotProduct(r.direction, r.direction);
	var b = Vector3d.dotProduct(r.direction, d);
	b *=2;
	var c= Vector3d.dotProduct(d,d)-(this.radius*this.radius);
	
	var discriminant = Math.sqrt(b*b-4*a*c);
	if(discriminant < 0){
		return undefined;
	}
	
	var t;
	if(discriminant < 1e-2){
		t = -b/(2*a);
	}else{
		t = (-b-discriminant)/(2*a);
	}
	
	return {
		t: t,
		normal: this.normal(r, t)
	}
}

Sphere.prototype.normal = function(r, t){
	var worldCoord = r.rayPoints(t);
	var nor = Vector3d.subtract(worldCoord, this.center);
	return nor.normalize();
}
