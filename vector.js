/*
 * Owner: Anuj Varma
 * 
 * This file has the description of 2d and 3d Vector that will be used rather frequently in 
 * all the files
 * 
 * need to make sure the definitions are perfect and all encompassing
 * 
 */

function Vector3d(x, y, z){
	this.x = x||0;
	this.y = y||0;
	this.z = z||0;
}

Vector3d.crossProduct = function(vec1, vec2){
	return new Vector3d(vec1.y*vec2.z-vec1.z*vec2.y, vec1.z*vec2.x-vec1.x*vec2.z, vec1.x*vec2.y-vec1.y*vec2.x);
}

Vector3d.dotProduct = function(vec1, vec2){
	return (vec1.x*vec2.x+vec1.y*vec2.y+vec1.z*vec2.z);
}

//What if i want to do the cross product and dot product with this vector?
Vector3d.prototype.dotProduct = function(vec){
	//Why am I returning.. oh well!!
	return (this.x*vec.x+this.y*vec.y+this.z*vec.z);
}

Vector3d.prototype.crossProduct = function(vec){
	return new Vector3d(this.y*vec.z-this.z*vec.y, this.z*vec.x-this.x*vec.z, this.x*vec.y-this.y*vec.x);
}

//Find the length of the current vector.. 
Vector3d.prototype.length = function(){
	return Math.sqrt(this.x*this.x+this.y*this.y+this.z*this.z);
}

Vector3d.prototype.normalize = function(){
	length = this.length();
	//prevent divide by zero errors
	if(length > 1e-4){
		this.x = this.x/length;
		this.y = this.y/length;
		this.z = this.z/length;
	}
	return this;
}

Vector3d.addVec = function(vec1, vec2){
	return new Vector3d(vec1.x+vec2.x, vec1.y+vec2.y, vec1.z+vec2.z);
}

Vector3d.prototype.addVec = function(vec){
	this.x += vec.x;
	this.y += vec.y;
	this.z += vec.z;
	return this;
}

Vector3d.prototype.copyVec = function(vec){
	this.x = vec.x;
	this.y = vec.y;
	this.z = vec.z;
	return this;
}

Vector3d.copyVec = function(vec){
	//just return a new vector
	return new Vector3d(vec.x, vec.y, vec.z);
}

Vector3d.prototype.mulVec = function(vec){
	this.x *= vec.x;
	this.y *= vec.y;
	this.z *= vec.z;
	return this;
}

Vector3d.mulVec = function(vec1, vec2){
	vec = new Vector3d(0,0,0);
	vec.x = vec1.x*vec2.x;
	vec.y = vec1.y*vec2.y;
	vec.z = vec1.z*vec2.z;
	return vec;
}

Vector3d.negate = function(v){
	return new Vector3d(-v.x, -v.y, -v.z);
}

Vector3d.prototype.negate = function(){
	this.x = -1*this.x;
	this.y = -1*this.y;
	this.z = -1*this.z;
	return this;
}

Vector3d.subtract = function(v1, v2){
	return new Vector3d(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z);
}

Vector3d.prototype.subtract = function(v){
	this.x -= v.x;
	this.y -= v.y;
	this.z -= v.z;
	return this;
}

//Scale a vector
Vector3d.scale = function(v, factor){
	return new Vector3d(v.x*factor, v.y*factor, v.z*factor);
}

Vector3d.prototype.scale = function(factor){
	this.x *= factor;
	this.y *= factor;
	this.z *= factor;
	return this;
}

Vector3d.addScale = function(vec1, vec2, factor){
	return new Vector3d(vec1.x+vec2.x*factor, vec1.y+vec2.y*factor, vec1.z+vec2.z*factor);
}

Vector3d.prototype.addScale = function(vec, factor){
	this.x += vec.x*factor;
	this.y += vec.y*factor;
	this.z += vec.z*factor;
	return this;
}

Vector3d.reflection = function(vec1, vec2){
	//Snells laws
	var vec2Scaled = Vector3d.copyVec(vec2).scale(Vector3d.dotProduct(vec1, vec2)*2);
	return Vector3d.subtract(vec1, vec2Scaled);
}

Vector3d.prototype.componentScale = function(factor) {
  this.x *= factor.x;
  this.y *= factor.y;
  this.z *= factor.z;
  return this;
}