/*
 * Owner: Anuj Varma
 * 
 * This Class defines all the objects that are handled by this raytracer.
 * 
 * Current implementation only supports Spheres, but soon with help of @Vaibhav and @Ali 
 * we will handle triangle and then will model the teapot 
 * 
 */
function raySceneIntersection(ray){
	var closestTVal = Infinity;
	var closestObj = undefined;
	var closestIntersect;	
	
	for(var i=0; i<gl_objects.length; i++){
		var intersection1 = gl_objects[i].intersection(ray);
		if(intersection1 && intersection1.t < closestTVal){
			closestTVal = intersection1.t;
			closestObj = gl_objects[i];
			closestIntersect = intersection1;
		}
	}
	
	if(closestObj == undefined){
		return undefined;
	}
	
	return {
		t: closestTVal,
		normal: closestIntersect.normal,
		obj: closestObj
	}
}

function Objects(material){
	this.material = material;
}

Objects.prototype.shade = function(ctx){
	//alert(ctx.normal);
	return this.material.evalShade(ctx);
}

Objects.prototype.intersection = function(r){
	throw 'intersect is not implemented';
}
