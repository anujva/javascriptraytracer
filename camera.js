/*
 * Owner : Anuj Varma
 * 
 * This file is going to model a camera class
 * 
 */

function Camera(campos, camdir, camright, camleft){
	this.camera_position = campos || new Vector3d(0,0,-10);
	this.camera_direction = camdir|| new Vector3d(0, 0, 1);
	this.camera_right = camright;
	this.camera_left = camleft;
}
