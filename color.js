/*
 * Owner: Anuj Varma
 * 
 * This file has the description of Color. This class borrows heavily from vectors file but will not be a derived class
 * 
 * need to make sure the definitions are perfect and all encompassing
 */

function Color(x, y, z, alpha){
	this.x = x||0;
	this.y = y||0;
	this.z = z||0;
	this.alpha = alpha||0;
}

Color.addVec = function(vec1, vec2){
	return new Color(vec1.x+vec2.x, vec1.y+vec2.y, vec1.z+vec2.z);
}

Color.prototype.addVec = function(vec){
	this.x += vec.x;
	this.y += vec.y;
	this.z += vec.z;
	return this;
}

Color.prototype.copyVec = function(vec){
	this.x = vec.x;
	this.y = vec.y;
	this.z = vec.z;
	return this;
}

Color.copyVec = function(vec){
	//just return a new vector
	return new Color(vec.x, vec.y, vec.z);
}

Color.prototype.mulVec = function(vec){
	this.x *= vec.x;
	this.y *= vec.y;
	this.z *= vec.z;
	return this;
}

Color.mulVec = function(vec1, vec2){
	vec = new Color(0,0,0);
	vec.x = vec1.x*vec2.x;
	vec.y = vec1.y*vec2.y;
	vec.z = vec1.z*vec2.z;
	return vec;
}

Color.subtract = function(v1, v2){
	return new Color(v1.x-v2.x, v1.y-v2.y, v1.z-v2.z);
}

Color.prototype.subtract = function(v){
	this.x -= v.x;
	this.y -= v.y;
	this.z -= v.z;
	return this;
}

//Scale a vector
Color.scale = function(v, factor){
	return new Color(v.x*factor, v.y*factor, v.z*factor);
}

Color.prototype.scale = function(factor){
	this.x *= factor;
	this.y *= factor;
	this.z *= factor;
	return this;
}

Color.addScale = function(vec1, vec2, factor){
	return new Color(vec1.x+vec2.x*factor, vec1.y+vec2.y*factor, vec1.z+vec2.z*factor);
}

Color.prototype.addScale = function(vec, factor){
	this.x += vec.x*factor;
	this.y += vec.y*factor;
	this.z += vec.z*factor;
	return this;
}

Color.prototype.componentScale = function(factor) {
  this.x *= factor.x;
  this.y *= factor.y;
  this.z *= factor.z;
  return this;
}