/*
 * Owner: Anuj Varma
 * 
 * 1. DirectionalLight
 * 
 */

function Lights(){
	this.lights = new Array();
}

Lights.prototype.eachLight = function(position, normal, javascriptCl, ctx){
	for(var i=0; i<this.lights.length; i++){
		var irrad = this.lights[i].evalLight(position, normal);
		
		//call the anonymous function
		javascriptCl.call(ctx, irrad)
	}
}

Lights.prototype.addNewLight = function(light){
	this.lights.push(light);
}


function Irradiance(dir, col, lightVis){
	this.direction = dir;
	this.color = col;
	this.lightVisible = lightVis;
}

function DirectionalLight(dir, col){
	this.direction = dir;
	this.color = col;
}

DirectionalLight.SHADOW_STEP = 10000;

DirectionalLight.prototype.evalLight = function(position, normal){
	var lightPos = Vector3d.addScale(position, this.direction, -DirectionalLight.SHADOW_STEP);
	var lightRay = new Ray(lightPos, this.direction);
	var shadTesting = raySceneIntersection(lightRay);
	
	var lightVis = Math.abs(shadTesting.t - DirectionalLight.SHADOW_STEP) < 1e-2;
	
	return new Irradiance(this.direction, this.color, lightVis);
}
