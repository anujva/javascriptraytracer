/******************************************************
 * Owner : Anuj Varma
 * Project : CSCI 580 final
 * 
 * This is the main script with the definition of the camera, the scene and the raytracer
 * 
 * 
 *  
 */

function get(f){
	return document.getElementById(f);
}

var canvas_width;
var canvas_height;
var ctx;

function runRaytracer(f){
	var canvas = get(f);
	canvas_width = canvas.width;
	canvas_height = canvas.height;
	ctx = canvas.getContext('2d');
	
	x = -10;
	y = 1;
	f = 0;
	//setup the eye (camera) and the screen
	setInterval(function(){
		x += 0.5;
		if(x == 10){
			x = -10;
		}
		y = (-0.5+ Math.sin(f))*1.5;
		f+=30;
		console.log(y);
		initialize_scene();
		clearCanvas(ctx);
		
		for(var i=0; i<canvas_height; i++){
			for(var j=0; j<canvas_width; j++){
				gl_depth.reset();
				
				while(gl_depth.hasNext()){
					var sample = gl_depth.getNext();
					
					//Shoot a ray from the camera/eye to this pixel
					var ray = shootRay(j+sample.x, i+sample.y);
					
					var intersection = raySceneIntersection(ray);
					
					if(intersection){
						var context_shading = {
							object: intersection.obj,
							t: intersection.t,
							ray:ray,
							normal: intersection.normal
						}
						if(context_shading.normal == undefined){
							alert("Context shading normal undefined")
						}
						var color = intersection.obj.shade(context_shading);
						gl_depth.addSamples(color);
					}
				}
				var pixCol = gl_depth.colorResult();
				var c = Vector3d.copyVec(pixCol);
				c.x = Math.max(0, Math.min(c.x, 1));
				c.y = Math.max(0, Math.min(c.y, 1));
				c.z = Math.max(0, Math.min(c.z, 1));
				c.x *= 255; 
				c.y *= 255; 
				c.z *= 255;
				ctx.fillStyle = 'rgb('+Math.floor(c.x)+','+ Math.floor(c.y) +','+ Math.floor(c.z)+')';
				ctx.fillRect(j, i, 1, 1);
			}
		}	
	}, 10);
}

//Function to shoot the ray
function shootRay(x, y){
	var width = canvas_width/2;
	var height = canvas_height/2;
	var ray_x = ((x-width)/width)*5;
	var ray_y = ((height-y)/height)*5;
	var d = new Vector3d(ray_x, ray_y, gl_near_plane-gl_camera.z);
	d.normalize();
	return new Ray(gl_camera, d);	
}

//clear the canvas for a new drawing
function clearCanvas(ctx){
	ctx.fillStyle = 'rgb(255, 255, 255)';
	ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function initialize_scene(){
	//lets initializa a few lights first;
	gl_lights = new Lights();
	gl_lights.addNewLight(new DirectionalLight(new Vector3d(0, -1, 0), new Color(1, 1, 1)));
	//gl_lights.addNewLight(new DirectionalLight(new Vector3d(0, 1, 50), new Color(1,1,1)));
	//Push all the objects of the scene onto this array
	gl_objects = new Array();
	
	//Create a new sphere
	var greenDiff = new LambertianMaterial(new Color(0, 1, 0), new Color(0, 0.4, 0));
	var greenSpec = new SpecularMaterial(new Color(1, 1, 1), 8, greenDiff);
	var redDiff = new LambertianMaterial(new Color(1, 0 ,0), new Color(0.2, 0, 0))
	var redSpec = new SpecularMaterial(new Color(1,1,1), 20, redDiff);
	var maroonDiff = new LambertianMaterial(new Color(0.68, 0.45, 0.23), new Color(0.2, 0.1, 0.06));
	var maroonSpec = new SpecularMaterial(new Color(1,1,1), 12, maroonDiff);
	var sphere1 = new Sphere(new Vector3d(x, y, 10), 2, greenSpec);
	var sphere2 = new Sphere(new Vector3d(4, -0.75, 2), 3, redSpec);
	gl_objects.push(sphere1);
	gl_objects.push(sphere2);
	var plane = new Plane(new Vector3d(0,1,0), -4, maroonSpec);
	//console.log(plane)
	gl_objects.push(plane);
	
	var triangle = new Triangle(new Vector3d(-1, 3, 5), new Vector3d(2, 3, 5), new Vector3d(0.5, 0, 5), greenSpec);
	console.log(triangle);
	//gl_objects.push(triangle);
	
	//define the world coordinates
	gl_X = new Vector3d(1,0,0);
	gl_Y = new Vector3d(0,1,0);
	gl_Z = new Vector3d(0,0,1);
	
	//How many points to sample per pixel?
	gl_depth = new Depth(1);
	gl_camera = new Vector3d(0, 0, -10);
	gl_camPos = new Vector3d(0,3,-10);
	gl_lookAt = new Vector3d(0,0,0);
	gl_cameraDir = (new Vector3d(gl_camPos.x - gl_lookAt.x, gl_camPos.y - gl_lookAt.y, gl_camPos.z - gl_lookAt.z)).negate().normalize();
	gl_camright = gl_Y.crossProduct(gl_cameraDir).normalize();
	gl_camdown = gl_camright.crossProduct(gl_cameraDir);
	
	//console.log(gl_camright);
	//console.log(gl_camdown);
	//console.log(gl_lookAt);
	//console.log(gl_camPos);
	
	gl_sceneCamera = new Camera(gl_camPos, gl_cameraDir, gl_camright, gl_camdown);
	//console.log(gl_sceneCamera);
	gl_near_plane = 0;
}

