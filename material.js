/*
 * Owner: Anuj Varma
 * 
 * In this file I am going to define a few fundamental types of materials
 * 
 * Superclass material will define two prototype functions : one for shading context and the other for radiance
 * 
 */

function Material(){
	
}

Material.prototype.evalShade = function(ctx){
	//return a color
	return new Vector3d();
}

Material.prototype.evalRad = function(irradiance, position, normal, ctx){
	//return a color
	return new Vector3d();
}


/*
 * Now I will begin to define 3 different kinds of material.
 * 1. Ambient Material : Defines how much ambient light is deflected by this material
 * 
 * 2. Lambertian Material : Defines how much diffuse light is reflected
 * 
 * 3. Specular Material : Specular materials create highlight on an object making them appear shiny. 
 * 
 */

function AmbientMaterial(col){
	this.col = col;
}

AmbientMaterial.prototype = new Material(); //Inheritance

AmbientMaterial.prototype.evalShade = function(ctx){
	return this.col;
}

AmbientMaterial.prototype.evalRad = function(irr, pos, nor, ctx){
	return this.col;
}

/*
 * LambertianMaterial
 */

function LambertianMaterial(col, ambientCol){
	this.col = col;
	
	//make the ambientCol parameter optional
	this.ambiCol = ambientCol || new Vector3d();
}

LambertianMaterial.prototype = new Material();

LambertianMaterial.prototype.evalShade = function(ctx){
	var worldCoord = ctx.ray.pointOnRay(ctx.t);
	var col= new Vector3d();
	gl_lights.eachLight(worldCoord, ctx.normal, function(incidentLight){
		var c = this.evalRad(incidentLight, worldCoord, ctx.normal);
		col.addVec(c);
	}, this);
	var ambient = Vector3d.componentScale(this.ambiCol, this.col);
	col.addVec(ambient);
	return col;
}

LambertianMaterial.prototype.evalRad = function(irradiance, position, normal, ctx){
	if(irradiance.lightVisible){
		var lambertian = Math.max(-Vector3d.dotProduct(normal, irradiance.direction), 0);
		var c = Vector3d.scale(irradiance.color, lambertian);
		c.componentScale(this.col);
		return c;
	}else{
		return new Vector3d();
	}
}

/*
 * Specular Material definition
 */

function SpecularMaterial(specCol, specPow, diffMaterial){
	this.specCol = specCol;
	this.specPow = specPow;
	this.diffmaterial = diffMaterial;
}

SpecularMaterial.prototype = new Material();

SpecularMaterial.prototype.evalShade = function(ctx){
	var worldCoord = ctx.ray.rayPoints(ctx.t);
	var diffCol = new Vector3d();
	var specCol = new Vector3d();
	
	gl_lights.eachLight(worldCoord, ctx.normal, function(incidentLight){
		//alert(ctx.normal);
		diffCol.addVec(this.diffmaterial.evalRad(incidentLight, worldCoord, ctx.normal, ctx));
		specCol.addVec(this.evalRad(incidentLight, worldCoord, ctx.normal, ctx));
	}, this);
	
	return Vector3d.addVec(diffCol, specCol);
}

SpecularMaterial.prototype.evalRad = function(irr, position, normal, ctx){
	var ref = Vector3d.reflection(ctx.ray.direction, normal);
	var pow = Math.pow(Math.max(-Vector3d.dotProduct(ref, irr.direction), 0), this.specPow);
	
	return Vector3d.scale(irr.color, pow);
}
